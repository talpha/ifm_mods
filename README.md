# IfM_mods

A digital record of physical changes I (Toby Harris) have made to the Alan Reece Building while here as a MET student, 2021-2023

Here you will find pictures and stories, but more importantly also CAD data and drawings should you wish to make more or further improve the designs. 

Projects will be made in git branches then merged into main. 